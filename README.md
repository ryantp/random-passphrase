# RPASS (Random-Passphrase Generator)
This is a standard passphrase generator. It uses standard python modules. The current iteration uses the standard ```random``` module for selecting characters. In the future, an implementation using ```os.urandom``` will be used.

Also in the distant future, a *GUI* version will be created for the Windows crowd. I know the terminal is scary ;) In all seriousness, I don't really do GUI apps, so that will be in the **very distant** future.

# --help text
    usage: 
    Make 20~ character password; no options required
    $ rpass
    > [passphrase]

    Make 20 character password, with exactly 20 characters
    $ rpass -e 20
    $ rpass --exact 20
    > [exactly 20 character passphrase]

    Make a randomly capitalized passphrase
    $ rpass -c 30
    $ rpass --caps-random 30
    > [randomly capitalized characters]

    Make a randomly leet speak passphrase
    $ rpass -l 30
    $ rpass --leet 30
    > [randomly leet-ed passphrase]

    Mix and match
    $ rpass -ecl 40
    $ rpass --exact --caps-random --leet 40
    > [fun times]

    NOTE:
    Passphrase length cannot be less than 12

    positional arguments:
      length             The length of the password. May be longer.

    optional arguments:
      -h, --help         show this help message and exit
      -c, --caps-random  Make random characters capitalized.
      -e, --exact        Make password exact length.
      -l, --leet         Make random characters leet-ed.


