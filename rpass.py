#!/usr/bin/python3

''' this is the CLI varient of this tool, for Linux specifically '''

import argparse
import os

import rpass_core as rpass

ROOT_DIR = os.path.realpath(os.path.dirname(__file__))
DICT_FILE = os.path.join(ROOT_DIR, 'rpass-dict')

PARSER_EXAMPLES = '''
Make 20~ character password; no options required
$ rpass
> [passphrase]

Make 20 character password, with exactly 20 characters
$ rpass -e 20
$ rpass --exact 20
> [exactly 20 character passphrase]

Make a randomly capitalized passphrase
$ rpass -c 30
$ rpass --caps-random 30
> [randomly capitalized characters]

Make a randomly leet speak passphrase
$ rpass -l 30
$ rpass --leet 30
> [randomly leet-ed passphrase]

Mix and match
$ rpass -ecl 40
$ rpass --exact --caps-random --leet 40
> [fun times]

NOTE:
Passphrase length cannot be less than {m}
'''.format(
	m = rpass.MIN_MAX_LENGTH)

parser = argparse.ArgumentParser(
	prog = "rpass-cli",
	usage = PARSER_EXAMPLES)

parser.add_argument(
	'length',
	action = 'store',
	nargs = '?',
	default = 20,
	type = int,
	help = "The length of the password. May be longer.")
parser.add_argument(
	'-c',
	'--caps-random',
	action = 'store_true',
	help = 'Make random characters capitalized.')
parser.add_argument(
	'-e',
	'--exact',
	action = 'store_true',
	help = 'Make password exact length.')
parser.add_argument(
	'-l',
	'--leet',
	action = 'store_true',
	help = 'Make random characters leet-ed.')

def main(length, caps, leet, exact):
	pphrase = rpass.create_passphrase(
		dict_file = DICT_FILE,
		min_length = length,
		rnd_caps = caps,
		mkleet = leet,
		exact = exact)
	print(pphrase)

if __name__ == "__main__":
	args = parser.parse_args()

	main(
		length = args.length,
		caps = args.caps_random,
		leet = args.leet,
		exact = args.exact)
