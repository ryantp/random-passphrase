#!/usr/bin/python3

''' This will be the core logic behind the UI, which will be split into CLI (for Linux, Unix, MacOS)
and a GUI (for windows). '''

import random
import string

SPCLC = "!@#$%^&*()-_=+?><." * 2
CHARS = string.ascii_letters + string.digits + SPCLC

L337 = {		# a simple leet converter
	'a': '@',
	'b': '6',
	'c': '(',
	'e': '3',
	'g': '9',
	'h': '#',
	'i': '1',
	'l': '|',
	'm': 'nn',
	'o': '0',
	's': '$',
	't': '7',
	'v': '<',
	'w': 'vv',
}

MIN_MAX_LENGTH = 12 # the create_passphrase min_length CANNOT be set lower than this!

def fetch_word(dict_file = None, min_length = 8):
	with open(dict_file, 'r') as f:
		words = f.read().split('\n')

	using = True
	while using:
		word = random.choice(words)
		if len(word) >= min_length:
			using = False

	word.replace("'", random.choice(SPCLC)) # just replace ' with a random special character
	return word

def random_capitals(word):
	POOL = ['p', 'p', 'p', 'c', 'l']
	new_word = ''

	for w in word:
		action = random.choice(POOL)
		if action == 'p':
			pass
		elif action == 'c':
			w = w.upper()
		else:
			w = w.lower()
		new_word += w

	return new_word

def make_leet(word):
	POOL = ['p', 'p', 'l']
	new_word = ''

	for w in word:
		action = random.choice(POOL)
		if action == 'p':
			pass
		else:
			try:
				w = L337[w.lower()]
			except KeyError:
				pass
		new_word += w

	return new_word

def fetch_char():
	return random.choice(CHARS)

def create_passphrase(dict_file = None, min_length = 20, rnd_caps = True, mkleet = False, exact = False):
	'''
	min_length = the minimum length the passphrase will be;
	rnd_caps = if the words generated will have random capitals
	mkleet = if words will be made leet
	exact = if the password will hold the exact length of min_length
	'''
	if min_length < MIN_MAX_LENGTH:
		min_length = MIN_MAX_LENGTH
	WC_POOL_MAX = int(min_length * 0.2)
	w_append = 0

	WORD_1OF5 = ['c', 'c', 'c', 'c', 'w']

	WORD = ''
	for i in range(min_length):
		action = random.choice(WORD_1OF5)
		if action == 'c':
			WORD += fetch_char()
		elif w_append < WC_POOL_MAX:
			w = fetch_word(dict_file = dict_file)
			if rnd_caps:
				w = random_capitals(w)

			if mkleet:
				w = make_leet(w)

			WORD += w
			w_append += 1

	if exact:
		if len(WORD) > min_length:
			WORD = WORD[:min_length]
	return WORD
